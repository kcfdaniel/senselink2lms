from time import time, sleep
import os
import requests
import hashlib
import json
import pandas as pd
import random
import numpy as np
import sys
from threading import Thread
import os
from datetime import datetime, date, timedelta
import pytz
import traceback
import argparse
from dotenv import load_dotenv
load_dotenv()

print()
print("started at", datetime.now())

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--device", 
                required=False, 
                default=0,
                type=int,
                help="device index")
ap.add_argument("-s", "--should-send-to-lms", 
                required=False, 
                default=True, 
                action='store_true')
ap.add_argument("-n", '--no-send', 
                required=False, 
                dest="should_send_to_lms", 
                action='store_false',
                help="do not send data to eclass")
ap.add_argument("-D", "--display", 
                required=False, 
                default=True, 
                action='store_true')
ap.add_argument("-N", '--no-display', 
                required=False, 
                dest="display", 
                action='store_false',
                help="do not display")
ap.add_argument("-p", "--position", 
                required=False, 
                default='', 
                help="position of the screen")
ap.add_argument("-l", "--left", 
                required=False, 
                dest="position", 
                action='store_const',
                const="left",
                help="display on the left")
ap.add_argument("-r", '--right', 
                required=False, 
                dest="position", 
                action='store_const',
                const="right",
                help="display on the right")
ap.add_argument("-L", "--lms", 
                required=False, 
                default='eclass', 
                help="learning management platform (eclass or grwth)")

args = vars(ap.parse_args())
initial_device_index = args["device"]
lms = args["lms"]

#senselink address
senselink_host = os.getenv("SENSELINK_HOST")
#Input app key
app_key = os.getenv("APP_KEY")
#Input app secret
app_secret = os.getenv("APP_SECRET")
#Image path
path = ""
#API URL
uri = "/api/v3/record/list"
# background image path
background_image_path = os.getenv("BACKGROUND_IMAGE_PATH")
# number of times to retry sending data to eclass
max_retry_number = 3
#send data to eclass or not
should_send_to_lms = args["should_send_to_lms"]
#GUI display enabled or not
display = args["display"]
#display screen on the left (True) or right (False)
screen_position = args["position"]

if lms == "eclass":
    #eclass licence key
    eclass_licence_key = os.getenv("ECLASS_LICENCE_KEY")
    # eclass attendence api endpoint
    eclass_api_endpoint = os.getenv("ECLASS_API_ENDPOINT")

card_id = "0000000000"
record_date = "00-00-00"
record_time = "00:00:00"
request_type = "ATT"
data = {"card_id": card_id,
        "record_date": record_date,
        "record_time": record_time,
        "request_type": request_type}

print("data:", json.dumps(data, indent=4))

headers = {"Authorization": "Bearer " + eclass_licence_key}

success = False
retry_count = 0
while not success:
    try:
        response = requests.post(eclass_api_endpoint, data=data, headers=headers)
        if response.status_code == 429 and retry_count < max_retry_number:
            # too many requests
            retry_count += 1
            sleep(1)
            continue
        success = True
    except requests.exceptions.ConnectionError:
        print(f"cannot connect to {eclass_api_endpoint}, check internet connection?")
        sleep(1)

print(response)
print(response.text)
