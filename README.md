# SenseLink To LMS <!-- omit in toc -->

## Table of Content <!-- omit in toc -->

- [Description](#description)
- [Compatibility](#compatibility)
- [Installation](#installation)
- [Usage](#usage)
  - [Changing the Working Directory](#changing-the-working-directory)
  - [Setting Up .env](#setting-up-env)
  - [Setting Up Auto Start and Restart Service for push_event_main.py](#setting-up-auto-start-and-restart-service-for-push_event_mainpy)
  - [(Legacy) Setting Up Auto Start and Restart Service for main.py](#legacy-setting-up-auto-start-and-restart-service-for-mainpy)
  - [(Legacy) Running main.py](#legacy-running-mainpy)
- [(Legacy) main.py Troubleshooting](#legacy-mainpy-troubleshooting)
  - [Display Issues (on Linux)](#display-issues-on-linux)
    - [Description](#description-1)
    - [Solution](#solution)

## Description

- `push_event_main.py`: listens for records from SenseLink and send to lms and emit to socket
- `save_records.py`: periodically pulls data (once per day at 00:00) from SenseLink and save as [YY-MM-DD].csv
- (legacy) `main.py`: periodically pulls data (once per second) from SenseLink and send to lms and display with GUI

## Compatibility

Python 3.6+

## Installation

**Linux/MacOS**:

```bash
pip3 install -r push_event_requirements.txt
```

**Windows**:

```bash
pip install -r push_event_requirements.txt
```

If using **save_records.py**, install using **save_records_requirements.txt** instead. i.e. `pip3 install -r save_records_requirements.txt` for Linux/MacOS or `pip install -r save_records_requirements.txt` for Windows.

If there is error installing PyQt5 on Linux, remove the PyQt5 line from **requirements.txt** and run `pip3 install -r requirements.txt` again, then install PyQt5 seperately with `sudo apt-get install python3-pyqt5`

(Legacy) If using **main.py**, install using **requirements.txt** instead. i.e. `pip3 install -r requirements.txt` for Linux/MacOS or `pip install -r requirements.txt` for Windows.

## Usage

### Changing the Working Directory

Open a terminal and change directory to the project root directory.

### Setting Up .env

Make a copy of .env.example as .env in the project root directory:

**Linux/MacOS**:

```bash
cp .env.example .env
```

**Windows**:

```bash
copy .env.example .env
```

Open .env in a text editor, set the following environment variables as needed:

#### SenseLink <!-- omit in toc -->

- `SENSELINK_HOST`: host address ond port of SenseLink
- `APP_KEY`: app key of SenseLink
- `APP_SECRET`: app secret of SenseLink

#### Host <!-- omit in toc -->

- `HOST_PORT`: host port of the local push event subscription service

#### eClass (optional, only if you are using eClass) <!-- omit in toc -->

- `ECLASS_LICENCE_KEY`: eClass licence key
- `ECLASS_API_ENDPOINT`: eClass API endpoint, should be "https://eclass-api2.eclasscloud.hk/api/student-attendance"

#### GRWTH (optional, only if you are using GRWTH) <!-- omit in toc -->

- `GRWTH_SCHOOL_ID`: GRWTH school ID (school code)
- `GRWTH_API_KEY`: GRWTH school API key
- `GRWTH_API_ENDPOINT`: GRWTH API endpoint, should be "https://app.grwth.hk/schoolpf/attendance/grwthADHUploadData"

#### Others (optional) <!-- omit in toc -->

- `BACKGROUND_IMAGE_PATH`: path of GUI background image

Save the .env file.

### Setting Up Auto Start and Restart Service for push_event_main.py

When running push_event_main.py on **cloud / remotely**:

```bash
python3 deploy.py
```

When running push_event_main.py on **the same subnet as SenseLink / locally**:

```bash
python3 deploy.py --local
```

If data should be sent to **GRWTH**, supply the `--lms grwth` argument:

```bash
python3 deploy.py --lms grwth
```

If data should **not** be **sent to any lms**, supply the `--no-send` argument:

```bash
python3 deploy.py --no-send
```

### (Legacy) Setting Up Auto Start and Restart Service for main.py

Make a copy of senselink2lms_example.sh as senselink2lms.sh in the project root directory:

```bash
cp senselink2lms_example.sh senselink2lms.sh
```

Make a copy of senselink2lms_example.service as senselink2lms.service in the project root directory:

```bash
cp senselink2lms_example.service senselink2lms.service
```

Open senselink2lms.service in a text editor, set `ExecStart`, `WorkingDirectory`, `User`, `XAUTHORITY`, `DISPLAY` as needed.  

The value of `ExecStart` should be the same as the result of this command:

```bash
echo `pwd`/senselink2lms.sh
```

The value of `WorkingDirectory` should be the same as the result of this command:

```bash
pwd
```

The value of `User` should be the same as the result of this command:

```bash
id -un
```

The value of `Environment=XAUTHORITY` should be the same as the result of this command:

```bash
echo /home/`id -un`/.Xauthority
```

Usually you don't have to change `DISPLAY`, leaving it as :0.0 is fine, but you can change it if the service cannot show the GUI.  `DISPLAY` should have the value same as the part starting from the `:` of the result of the following command:

```bash
echo "$DISPLAY"
```

Save the senselink2lms.service file.

### (Legacy) Running main.py

If you simply want to start the **main.py** program manually once, you can simply use `python3 main.py` on Linux/MacOS or `python main.py` on Windows.  

Use the `--lms` argument if you want you use GRWTH as the LMS, e.g. on Linux/MacOS:

```bash
python3 main.py --lms grwth
```

If you want to auto start the program on boot and restart on failure (on Linux), execute:

```bash
./auto_start_on_linux_example.sh
```

Edit auto_start_on_linux_example.sh as needed.

For running push_event_main.py, use `python3 push_event_main.py` on Linux/MacOS or `python push_event_main.py` on Windows.  

## (Legacy) main.py Troubleshooting

### Display Issues (on Linux)

#### Description

qt.qpa.screen: QXcbConnection: Could not connect to display :0.0
Could not connect to any X display.

#### Solution

If the display 0.0 cannot be used, check the default display that should be used with terminal shell command:

```bash
echo $DISPLAY
```

Change the following line in senselink2lms.service:

```bash
Environment=DISPLAY=:0.0
```

E.g. if `echo $DISPLAY` shows `:1`, then change it to

```bash
Environment=DISPLAY=:1
```

