import os
import requests
import pandas as pd
import numpy as np
import sys
import os
from datetime import datetime, date
from dotenv import load_dotenv
load_dotenv()
import pandas as pd
import json
from time import sleep

#eclass licence key
eclass_licence_key = os.getenv("ECLASS_LICENCE_KEY")
# eclass attendence api endpoint
eclass_api_endpoint = os.getenv("ECLASS_API_ENDPOINT")

#temperature threshold
temperature_threshold = 38

# number of times to retry sending data to eclass
max_retry_number = 3

unregistered_cards_file = open("unregistered_cards.txt", "w")

try:
    df = pd.read_csv('eclass.csv')
except FileNotFoundError:
    print("Error: eclass.csv not found, ensure that you have this file in the same directory as this script")
    sys.exit()

for idx, item in df.iterrows():
    card_id = str(int(item['Card ID'])).zfill(10)
    record_date = item['Date']
    record_time = item['Time']
    request_type = "ATT"
    temperature = item['Temperature']
    location = item['Site']

    data = {"card_id": card_id,
        "record_date": record_date,
        "record_time": record_time,
        "request_type": request_type}

    if temperature and not np.isnan(temperature) and temperature != 0:
        data["temperature"] = temperature
        temperature_status = 1 if temperature >= temperature_threshold else 0
        data["temperature_status"] = temperature_status

    if location:
        data["location"] = location[:16]

    print("data:", json.dumps(data, indent=4))

    headers = {"Authorization": "Bearer " + eclass_licence_key}

    success = False
    retry_count = 0
    while not success:
        try:
            response = requests.post(eclass_api_endpoint, data=data, headers=headers)
            if response.status_code == 429 and retry_count < max_retry_number:
                # too many requests
                retry_count += 1
                sleep(1)
                continue
            elif response.status_code == 422:
                # card not registered
                print("Uncaught exception:")
                unregistered_cards_file.write(data['card_id']+"\n")
            success = True
        except requests.exceptions.ConnectionError:
            print(f"cannot connect to {eclass_api_endpoint}, check internet connection?")
            sleep(1)

    print(response)
    print(response.text)

unregistered_cards_file.close()