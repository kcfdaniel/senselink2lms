from time import time, sleep
import os
# from gevent import monkey, sleep
# monkey.patch_all()
import requests
import hashlib
import json
import numpy as np
import sys
from threading import Thread
import os
from datetime import datetime, date, timedelta
import pytz
from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_socketio import SocketIO, send, emit
import socket
parser = reqparse.RequestParser()
# from gevent.pywsgi import WSGIServer
import traceback
import argparse
from dotenv import load_dotenv
load_dotenv()
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

print()
print("started at", datetime.now())

ap = argparse.ArgumentParser()
ap.add_argument("-s", "--should-send-to-lms",
                required=False,
                default=True,
                action='store_true')
ap.add_argument("-n", '--no-send',
                required=False,
                dest="should_send_to_lms",
                action='store_false',
                help="do not send data to eclass")
ap.add_argument("-L", "--lms",
                required=False,
                default='eclass',
                help="learning management platform (eclass or grwth)")
ap.add_argument("-r", "--remote",
                required=False,
                default=True,
                action='store_true',
                help="executing in a different subnet in regards to SenseLink")
ap.add_argument("-l", '--local',
                required=False,
                dest="remote",
                action='store_false',
                help="executing in the same subnet in regards to SenseLink")
ap.add_argument("-v", '--verbose',
                required=False,
                default=False,
                action='store_true',
                help="print more messages for debugging")

args = vars(ap.parse_args())
# the lms used
lms = args["lms"]

# senselink host
senselink_host = os.getenv("SENSELINK_HOST")
# host port
host_port = int(os.getenv("HOST_PORT"))
# Input app key
app_key = os.getenv("APP_KEY")
# Input app secret
app_secret = os.getenv("APP_SECRET")
# Image path
path = ""

# subscribe uri
subscribe_uri = "/api/v3/event/updateSub"
# check subscription uri
check_subscription_uri = "/api/v3/event/viewSub"

# API URL
uri = "/api/v3/record/list"
# background image path
background_image_path = os.getenv("BACKGROUND_IMAGE_PATH")
# number of times to retry sending data to eclass
max_retry_number = 5
# send data to lms or not
should_send_to_lms = args["should_send_to_lms"]
# executing remotely or locally in regards to senselink
remote = args["remote"]
# verbose: print more debug messages
verbose = args["verbose"]

if lms == "eclass":
    #eclass licence key
    eclass_licence_key = os.getenv("ECLASS_LICENCE_KEY")
    # eclass attendence api endpoint
    eclass_api_endpoint = os.getenv("ECLASS_API_ENDPOINT")

if lms == "grwth":
    grwth_school_id = os.getenv("GRWTH_SCHOOL_ID")
    grwth_api_key = os.getenv("GRWTH_API_KEY")
    grwth_api_endpoint = os.getenv("GRWTH_API_ENDPOINT")

#temperature threshold
temperature_threshold = 38

def send_to_eclass(dict_data):
    card_id = str(dict_data['icNumber']).zfill(10)
    #record_date = str(dict_data['signDate'])
    record_date = str(datetime.fromtimestamp(int(dict_data['signTime']),pytz.timezone('Hongkong')).strftime('%Y:%m:%d'))
    record_time = str(datetime.fromtimestamp(int(dict_data['signTime']),pytz.timezone('Hongkong')).strftime('%H:%M:%S'))
    request_type = "ATT"
    data = {"card_id": card_id,
            "record_date": record_date,
            "record_time": record_time,
            "request_type": request_type}

    # include temperature data if present
    if 'bodyTemperature' in dict_data and dict_data['bodyTemperature'] != 0:
        temperature = dict_data['bodyTemperature']
        temperature_status = 1 if temperature >= temperature_threshold else 0
        data["temperature"] = float(temperature)
        data["temperature_status"] = temperature_status

    # include location data if present
    location = dict_data['location']
    if location:
        data["location"] = location[:16]

    print()
    print("current time:", datetime.now())
    print("data:", json.dumps(data, indent=4))

    headers = {"Authorization": "Bearer " + eclass_licence_key}

    # try to send data to eclass
    success = False
    retry_count = 0
    while not success:
        try:
            response = requests.post(eclass_api_endpoint, data=data, headers=headers)
            print(response)
            print(response.text)
            if (response.status_code == 429 or response.status_code == 502) and retry_count < max_retry_number:
                # too many requests or bad gateway
                retry_count += 1
                sleep(1)
                continue
            success = True
        except requests.exceptions.ConnectionError:
            print(f"cannot connect to {eclass_api_endpoint}, check internet connection?")
            sleep(1)

def get_timestamp():
    return str(round(time()) * 1000)

def get_sign(timestamp):
    hl = hashlib.md5()
    hl.update((timestamp + '#' +app_secret).encode(encoding='utf-8'))
    return hl.hexdigest()

def call_senselink_api(url, method='get', params={}, data={}):
    timestamp = get_timestamp()
    sign = get_sign(timestamp)
    auth_params = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
    }

    params = {**params, **auth_params}

    success = False
    retry_count = 0
    connection_error = False
    while not success:
        try:
            if method == "get":
                response = requests.get(url, params=params, timeout=1,verify=False)
            elif method == "post":
                response = requests.post(url, params=params, json=data, timeout=1)
            else:
                print(f"method {method} not supported when calling senselink api")
            # print(response)
            # print(response.text)
            if (response.status_code == 429 or response.status_code == 502) and retry_count < max_retry_number:
                # too many requests or bad gateway
                retry_count += 1
                sleep(1)
                continue
            success = True
        except requests.exceptions.ConnectionError:
            if not connection_error:
                connection_error = True
                print(f"cannot connect to {url}, retrying", end="")
            else:
                print(".", end="")
            sleep(1)
        except requests.exceptions.Timeout:
            if retry_count < max_retry_number:
                retry_count += 1
                print("request timeout, trying again")
            else:
                print("ERROR: multiple request timeouts, giving up")
                success = True
    try:
        return json.loads(response.text)
    except json.decoder.JSONDecodeError:
        return {}


def subscribe():
    print("subscribe!")
    timestamp = get_timestamp()
    sign = get_sign(timestamp)
    url = senselink_host + subscribe_uri
    params = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
    }

    if remote:
        ip = requests.get('https://checkip.amazonaws.com',verify=False).text.strip()
    else:
        # reference: https://stackoverflow.com/questions/24196932/how-can-i-get-the-ip-address-from-nic-in-python
        def get_ip_address():
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("8.8.8.8", 80))
            return s.getsockname()[0]
        ip = get_ip_address()

    data = {
        "event_type_ids": [30000, 30100],
        "event_dest": f"http://{ip}:{host_port}/eventRcv"
    }

    # try to subscribe to SenseLink
    success = False
    retry_count = 0
    connection_error = False
    while not success:
        try:
            response = requests.post(url, params=params, json=data)
            print(response)
            print(response.text)
            if (response.status_code == 429 or response.status_code == 502) and retry_count < max_retry_number:
                # too many requests or bad gateway
                retry_count += 1
                sleep(1)
                continue
            success = True
        except requests.exceptions.ConnectionError:
            if not connection_error:
                connection_error = True
                print(f"cannot connect to {url}, retrying", end="")
            else:
                print(".", end="")
            sleep(1)

def check_subscription():
    timestamp = get_timestamp()
    sign = get_sign(timestamp)
    url = senselink_host + check_subscription_uri
    params = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
    }

    result = requests.get(url, params=params,verify=False)
    data = json.loads(result.text)
    print(data)

subscribe()
# check_subscription()

prev_record = {}
current_record = {}
combined_record = {}

app = Flask(__name__)
api = Api(app)
socketio = SocketIO(app, cors_allowed_origins="*")

def combine_record():
    global prev_record
    global current_record
    global combined_record

    current_record['icNumber'] = current_record['icNumber'] or current_record['userIcNumber']
    current_record = {k: v for k, v in current_record.items() if v}
    if 'bodyTemperature' in current_record and current_record['bodyTemperature'] == 0: del current_record['bodyTemperature']

    if (
        prev_record and # prev_record exist
        int(current_record['signTime']) - int(prev_record['signTime']) <= 4 and  # records time proximity
            (not ('icNumber' in current_record) and ('icNumber' in prev_record) or # only prev_record has icNumber
            not ('icNumber' in prev_record) and ('icNumber' in current_record) or # only current_record has icNumber
            'icNumber' in prev_record and 'icNumber' in current_record and prev_record['icNumber'] == current_record['icNumber']) # both records have icNumber and are the same
        ):
            combined_record = {**prev_record, **current_record}
            # print(json.dumps(combined_record, indent=4))
    else:
        # combined_record might not have icNumber here
        combined_record = current_record

    return combined_record

class Receive_Event_Resource(Resource):
    def post(self):
        global prev_record
        global current_record
        global combined_record

        event = request.get_json(force=True)

        # if receive recognition record
        if event['eventType'] == 30000:
            current_record = event['data']
            # print(json.dumps(current_record, indent=4, ensure_ascii=False))

            combined_record = combine_record()

            #emit to socket
            socketio.emit('on_new_record', combined_record)

            print()
            if os.name == "nt":
                # avoid error on windows
                print("combined record:", str(combined_record).encode('utf-8'))
            else:
                print("combined record:", combined_record)

            if lms == "eclass" and should_send_to_lms and 'icNumber' in combined_record:
                send_to_eclass(combined_record)
            elif lms == "grwth" and should_send_to_lms and 'icNumber' in combined_record:
                send_to_grwth(combined_record)

            prev_record = current_record
            return {'message': 'OK'}, 200
        # if device alarm
        elif event['eventType'] == 30100 and event['data']['code'] == 20003 and event['data']['status'] == 1:
            print()
            print("FIRE ALARM!")
            data = event['data']
            if os.name == "nt":
                # avoid error on windows
                print("data:", str(data).encode('utf-8'))
            else:
                print("data:", data)
            handle_fire_alarm(event['data']['deviceId'])


api.add_resource(Receive_Event_Resource, '/eventRcv')

'''
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
'''

def set_keep_door_open_duration_for_all_devices(keep_door_open_duration):
    print("Settings all door open duration for all devices to", keep_door_open_duration)
    url = senselink_host + "/api/v3/device"
    response = call_senselink_api(url=url)

    if not response:
        print("ERROR: cannot get device list")
        return

    device_list = response['data']['device_list']
    print("device_list", device_list)
    device_ids = [device["id"] for device in device_list]
    set_keep_door_open_duration_for_devices(device_ids, keep_door_open_duration)

def set_keep_door_open_duration_for_devices(device_ids, keep_door_open_duration):
    print(f"set_keep_door_open_duration_for_devices {device_ids} to {keep_door_open_duration} seconds")
    if not device_ids:
        print("Warning: device_ids is empty when calling open_devices_door")
        return
    url = senselink_host + "/api/v2/device/update/config/batch"
    # device_ids = [10221] # for testing only
    params = {
    }
    data = {
        "device_ids": device_ids,
        "spse": {
            "keep_door_open_duration": keep_door_open_duration,
            "gpio_b": 4,
        },
    }
    response = call_senselink_api(url=url, method="post", params=params, data=data)
    print("response", response)
    #print("GPIO-B Status", data['gpio_b'])
    #gpiob_status = data['gpio_b']

# set_keep_door_open_duration_for_all_devices(30)

def handle_fire_alarm(device_id):
    url = senselink_host + "/api/v3/device"
    response = call_senselink_api(url=url)

    if not response:
        print("ERROR: cannot get device list")
        return

    device_list = response['data']['device_list']
    print("device_list", device_list)
    online_device_ids = [device["id"] for device in device_list if device["state"] == 1]
    set_keep_door_open_duration_for_devices(online_device_ids, 10)
    open_devices_door(device_id, online_device_ids)
    #dismiss_alarm()

def open_devices_door(alarm_device_id, device_ids=[]):
    if not device_ids:
        print("Warning: device_ids is empty when calling open_devices_door")
        return
    url = senselink_host + "/api/v2/device/open"
    # device_ids = [10221] # for testing only
    while True:
        for device_id in device_ids:
            print("opening door for device", device_id)
            params = {"id": device_id}
            response = call_senselink_api(url=url, method="post", params=params)
            print("response", response)
        sleep(3) # sleep 3 seconds ok for 1 device

        #check if alarm is still on
        check_alarm_status_url = senselink_host + "/api/v2/device/alarm/list"
        params = {
            "deviceId": alarm_device_id,
            "code": 20003,
        }
        response = call_senselink_api(url=check_alarm_status_url, method="get", params=params)
        print("response", response)
        if response:
            current_alarm_code = response['data']['data'][0]['code']
            current_alarm_id = response['data']['data'][0]['traceId']
            current_alarm_status = response['data']['data'][0]['status']
            current_alarm_timestamp = response['data']['data'][0]['alarmTimeL']

            #while current_alarm_id is not None:
            if current_alarm_status == 1 and current_alarm_code == 20003:
                if int(get_timestamp()) - current_alarm_timestamp > 5  : # alarm still remains alert after 5 seconds
                    #print(current_alarm_id)
                    dismiss_alarm(current_alarm_id)
                    #gpio_b = 4
                    remote_set_GPIO_B_input_status(4)
                    if current_alarm_id is None:
                        break

            if current_alarm_status == 3:
                print("Fire alarm is dismissed")
                    #break
        else:
            print("ERROR: failed to query alarms")
            return

def dismiss_alarm(current_alarm_id):
    dismiss_alarm_url = senselink_host + "/api/v2/device/alarm/disarm"
    params = {
            "traceId":current_alarm_id,
        }
    response = call_senselink_api(url=dismiss_alarm_url, method="post", params=params)
    print("response", response)

def remote_set_GPIO_B_input_status(gpiob_status):
    url = senselink_host + "/api/v2/device/update/config/batch"
    params = {
    }
    data = {
        "spse": {
            "gpio_b": 4,
        },
    }
    response = call_senselink_api(url=url, method="post", params=params, data=data)
    #print("response", response)
    gpiob_status = data['spse']['gpio_b']
    print("GPIO-B Status", gpiob_status)
    return gpiob_status

def send_to_grwth(dict_data):
    card_id = str(dict_data['icNumber']).zfill(10)
    record_date = str(dict_data['signDate'])
    device_ldid = str(dict_data['sn'])
    record_time = str(datetime.fromtimestamp(dict_data['signTime'],pytz.timezone('Hongkong')).strftime('%Y%m%d %H:%M:%S'))
    data =  {
        "dat":[
            {
                "rid": device_ldid,
                "cid": card_id,
                "time": record_time
            }
        ]
    }
    print("data:", json.dumps(data, indent=4))
    data = str(data)

    string_to_sign = f"{data}{grwth_school_id}{grwth_api_key}"
    print("string to sign:", string_to_sign)
    sign = hashlib.md5(string_to_sign.encode('utf-8')).hexdigest()

    headers = {
        "sign": sign,
        "schoolcode": grwth_school_id
    }

    print("headers:", headers)

    success = False
    retry_count = 0
    while not success:
        try:
            response = requests.post(grwth_api_endpoint, data=data, headers=headers)
            print(response)
            print(response.text)
            if (response.status_code == 429 or response.status_code == 502) and retry_count < max_retry_number:
                # too many requests or bad gateway
                retry_count += 1
                sleep(1)
                continue
            success = True
        except requests.exceptions.ConnectionError:
            print(f"cannot connect to {grwth_api_endpoint}, check internet connection?")
            sleep(1)

@socketio.on('connect')
def on_client_connect():
    print("Client connected")

@socketio.on('disconnect')
def on_client_disconnect():
    print('Client disconnected')

# http_server = WSGIServer(('0.0.0.0', host_port), app)
# http_server.serve_forever()

socketio.run(app, port=host_port, host='0.0.0.0')
