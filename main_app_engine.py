from time import time, sleep
import os
import requests
import hashlib
import json
import pandas as pd
import random
import numpy as np
import sys
from threading import Thread
import os
from datetime import datetime, date, timedelta
import traceback
from dotenv import load_dotenv
load_dotenv()

print()
print("started at", datetime.now())

#initial device index
initial_device_index = 0
#lms (learning management system used)
lms = "eclass"
#senselink address
senselink_host = os.getenv("SENSELINK_HOST")
#Input app key
app_key = os.getenv("APP_KEY")
#Input app secret
app_secret = os.getenv("APP_SECRET")
#Image path
path = ""
#SenseLink recording listing API URL
uri = "/api/v3/record/list"
# background image path
background_image_path = os.getenv("BACKGROUND_IMAGE_PATH")
# number of times to retry sending data to eclass
max_retry_number = 3
#send data to eclass or not
should_send_to_lms = True
#GUI display enabled or not
display = False
#display screen on the left (True) or right (False)
screen_position = ""


if __name__=="__main__":
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument("-d", "--device", 
                    required=False, 
                    default=0,
                    type=int,
                    help="device index")
    ap.add_argument("-s", "--should-send-to-lms", 
                    required=False, 
                    default=True, 
                    action='store_true')
    ap.add_argument("-n", '--no-send', 
                    required=False, 
                    dest="should_send_to_lms", 
                    action='store_false',
                    help="do not send data to eclass")
    ap.add_argument("-D", "--display", 
                    required=False, 
                    default=True, 
                    action='store_true')
    ap.add_argument("-N", '--no-display', 
                    required=False, 
                    dest="display", 
                    action='store_false',
                    help="do not display")
    ap.add_argument("-p", "--position", 
                    required=False, 
                    default='', 
                    help="position of the screen")
    ap.add_argument("-l", "--left", 
                    required=False, 
                    dest="position", 
                    action='store_const',
                    const="left",
                    help="display on the left")
    ap.add_argument("-r", '--right', 
                    required=False, 
                    dest="position", 
                    action='store_const',
                    const="right",
                    help="display on the right")
    ap.add_argument("-L", "--lms", 
                    required=False, 
                    default='eclass', 
                    help="learning management platform (eclass or grwth)")

    args = vars(ap.parse_args())

    initial_device_index = args["device"]
    lms = args["lms"]
    should_send_to_lms = args["should_send_to_lms"]
    display = args["display"]
    screen_position = args["position"]

if lms == "eclass":
    #eclass licence key
    eclass_licence_key = os.getenv("ECLASS_LICENCE_KEY")
    # eclass attendence api endpoint
    eclass_api_endpoint = os.getenv("ECLASS_API_ENDPOINT")

if lms == "grwth":
    grwth_school_id = os.getenv("GRWTH_SCHOOL_ID")
    grwth_api_key = os.getenv("GRWTH_API_KEY")
    grwth_api_endpoint = os.getenv("GRWTH_API_ENDPOINT")

if display:
    from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QLabel, QMainWindow, QComboBox, QDesktopWidget
    from PyQt5.QtGui import QFont, QFontMetrics
    from PyQt5.QtWidgets import QApplication
    from PyQt5.QtCore import QThread, Qt

def try_to_get_devices_from_senselink():
    timestamp = getTimestamp()
    sign = getSign(timestamp)
    url = senselink_host + '/api/v3/device'
    headers = {
        "Origin": "https://www.mywbsite.fr",
        "requestOrigin": "https://www.mywbsite.fr",
        "requestDomain": "https://www.mywbsite.fr",
        "Host": "https://www.mywbsite.fr",
    }
    data = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
    }
    return requests.get(url, params=data)

def get_devices():
    success = False
    while not success:
        try:
            result = try_to_get_devices_from_senselink()
            data = json.loads(result.text, encoding = 'utf_8_sig')
            # print(json.dumps(data, indent=4))
            if not result.ok and 'data' in data:
                print('looks like senselink is starting but not ready yet, please wait for a few more minutes')
                sleep(1)
                continue
            success = True
        except requests.exceptions.ConnectionError:
            print(f"cannot connect to senselink, check internet connection?")
            sleep(1)
        except json.decoder.JSONDecodeError:
            print("can't decode to json:")
            print(result.text)
            sleep(1)

    try:
        return [{'ldid': device['ldid'], 'name': device['name']} for device in data['data']['device_list']]
    except:
        print("failed to get devices")
        print(json.dumps(data, indent=4))
        return [{'ldid': "", 'name': "n/a"}]

#late time threshold
def get_late_time_threshold():
    return datetime.now().replace(hour=8, minute=0, second=0, microsecond=0)

#temperature threshold
temperature_threshold = 38

#list storing user names
names = []

#today's date
today = date.today()

#for filtering out first record
first_data = False

def getTimestamp():
    return str(round(time()) * 1000)

def getSign(timestamp):
    hl = hashlib.md5()
    hl.update((timestamp + '#' +app_secret).encode(encoding='utf-8'))
    return hl.hexdigest()

def find_nearest_temp_top(idx, item, df):
    if idx == 0:
        if item['body_temperature'] is not None and item['body_temperature'] != 0.0 and item['user_id'] == 0:
            return item['body_temperature'], 0
        else:
            return 37.0, 0
    elif df.iloc[idx-1]['body_temperature'] is not None and df.iloc[idx-1]['body_temperature'] != 0.0 and df.iloc[idx-1]['user_id'] == 0 :
        return df.iloc[idx-1]['body_temperature'], idx-1
    else:
        return find_nearest_temp_top(idx-1, item, df)

def find_nearest_temp_down(idx, item, df):
    if idx == len(df)-1:
        if item['body_temperature'] is not None and item['body_temperature'] != 0.0 and item['user_id'] == 0:
            return item['body_temperature'], len(df)-1
        else:
            return 37.0, len(df)-1
    elif df.iloc[idx+1]['body_temperature'] is not None and df.iloc[idx+1]['body_temperature'] != 0.0 and df.iloc[idx+1]['user_id'] == 0 :
        return df.iloc[idx+1]['body_temperature'], idx+1
    else:
        return find_nearest_temp_down(idx+1, item, df)


def compare(idx, item, df, top_idx, down_idx):
    top_time_diff = df.iloc[top_idx]['sign_time'] - item['sign_time']
    down_time_diff = item['sign_time'] - df.iloc[down_idx]['sign_time']

    if top_time_diff <= down_time_diff:
        return 1
    else:
        return 0

def time_range_exceed_four_seconds(df, idx, compare_idx):
    time_diff = abs(df.iloc[idx]['sign_time'] - df.iloc[compare_idx]['sign_time'])
    if time_diff >= 4:
        return True
    else:
        return False

if display:
    class Main_GUI(QMainWindow):
        def __init__(self):
            super().__init__()
            self.resize(800, 400)

            # self.setStyleSheet("background-color:#ff0000;")

            self.centralWidget = QWidget()
            self.setCentralWidget(self.centralWidget)

            layout = QVBoxLayout()
            self.centralWidget.setLayout(layout)

            devices_select_layout = QHBoxLayout()
            current_layout = QVBoxLayout()
            head_count_layout = QHBoxLayout()
            #prev1_layout = QVBoxLayout()
            #prev2_layout = QVBoxLayout()

            layout.addLayout(devices_select_layout)
            layout.addStretch(1)
            layout.addLayout(current_layout)
            layout.addLayout(head_count_layout)
            layout.addStretch(1)
            #layout.addLayout(prev1_layout)
            #layout.addLayout(prev2_layout)

            device_label = QLabel()
            device_label.setText("Device: ")
            self.device_selection_box = QComboBox()
            width = self.device_selection_box.sizeHint().width()
            if width < 200:
                self.device_selection_box.setFixedWidth(200)
            self.device_selection_box.addItems([device['name'] for device in devices])
            self.device_selection_box.currentIndexChanged.connect(self.device_selection_change)

            devices_select_layout.addStretch(1)
            devices_select_layout.addWidget(device_label)
            devices_select_layout.addWidget(self.device_selection_box)

            current_name_temperature_font = QFont('Open Sans', 70, QFont.Bold)
            current_time_font = QFont('Open Sans', 50, QFont.Bold)

            self.current_name_temperature_label = QLabel()
            self.current_name_temperature_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
            self.current_name_temperature_label.setFont(current_name_temperature_font)
            current_layout.addWidget(self.current_name_temperature_label)

            self.current_time_label = QLabel()
            self.current_time_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
            self.current_time_label.setFont(current_time_font)
            current_layout.addWidget(self.current_time_label)

            head_count_font = QFont('Open Sans', 30, QFont.Bold)
            self.head_count_label = QLabel()
            self.head_count_label.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
            self.head_count_label.setFont(head_count_font)
            self.head_count_label.setText('人數: ' + str(len(names)))
            head_count_layout.addStretch(5)
            head_count_layout.addWidget(self.head_count_label)
            head_count_layout.addStretch(1)

            '''
            prev1_layout.addStretch(1)

            prev_name_temperature_font = QFont('Open Sans', 40, QFont.Bold)
            prev_time_font = QFont('Open Sans', 30, QFont.Bold)

            self.prev1_name_temperature_label = QLabel()
            self.prev1_name_temperature_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
            self.prev1_name_temperature_label.setFont(prev_name_temperature_font)
            prev1_layout.addWidget(self.prev1_name_temperature_label)

            self.prev1_time_label = QLabel()
            self.prev1_time_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
            self.prev1_time_label.setFont(prev_time_font)
            prev1_layout.addWidget(self.prev1_time_label)

            self.prev2_name_temperature_label = QLabel()
            self.prev2_name_temperature_label.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
            self.prev2_name_temperature_label.setFont(prev_name_temperature_font)
            prev2_layout.addWidget(self.prev2_name_temperature_label)

            self.prev2_time_label = QLabel()
            self.prev2_time_label.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
            self.prev2_time_label.setFont(prev_time_font)
            prev2_layout.addWidget(self.prev2_time_label)

            prev2_layout.addStretch(1)
            '''

        def position_on_screen(self):
            if screen_position == "left":
                available_geometry = QDesktopWidget().availableGeometry()
                self.move(0,0)
                self.setFixedSize(available_geometry.width()/2, available_geometry.height())
            elif screen_position == "right":
                available_geometry = QDesktopWidget().availableGeometry()
                self.move(available_geometry.width()/2, 0)
                self.setFixedSize(available_geometry.width()/2, available_geometry.height())

        def device_selection_change(self, index):
            global current_device_id
            global first_data
            current_device_id = devices[index]['ldid']
            first_data = True

current_name_temperature_datetime = ('', None, None)
prev_name_temperature_datetime1 = ('', None, None)
prev_name_temperature_datetime2 = ('', None, None)

start_datetime = "2020-06-04 15:45:21"
end_datetime = "2020-06-04 23:45:21"
def all():
    global current_name_temperature_datetime
    global prev_name_temperature_datetime1
    global prev_name_temperature_datetime2
    global today
    global names
    global first_data

    global start_datetime
    global end_datetime

    timestamp = getTimestamp()
    sign = getSign(timestamp)
    url = senselink_host + uri
    data = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
        "device_sn": current_device_id,
        "date_time_from": "2020-07-20 0:45:21", #only for testing, should be commented
        "date_time_to": "2020-08-20 09:16:21", #only for testing, should be commented
    }
    # files = {
    #     "face_avatar": (str.split(path, "/")[-1], open(path, "rb"), "image/jpeg")
    # }

    # data["date_time_from"] = start_datetime
    # data["date_time_to"] = end_datetime
    # start_datetime = (datetime.strptime(start_datetime, '%Y-%m-%d %H:%M:%S') + timedelta(seconds = 1)).strftime('%Y-%m-%d %H:%M:%S')
    # end_datetime = (datetime.strptime(end_datetime, '%Y-%m-%d %H:%M:%S') + timedelta(seconds = 1)).strftime('%Y-%m-%d %H:%M:%S')

    success = False
    while not success:
        try:
            result = requests.get(url, params=data)
            data = json.loads(result.text, encoding = 'utf_8_sig')
            # print(json.dumps(data, indent=4))
            if not result.ok:
                print('looks like senselink is starting but not ready yet, please wait for a few more minutes')
                sleep(1)
                continue
            success = True
        except requests.exceptions.ConnectionError:
            print(f"cannot connect to {url}, check internet connection?")
            sleep(1)
        except json.decoder.JSONDecodeError:
            print("can't decode to json when getting records:")
            print(result.text)
            sleep(1)

    # result = json.dumps(json.loads(result.text), indent=4)
    # print(result)

    #df = pd.read_json(data['data']['data']['record_list'])

    try:
        df = pd.DataFrame.from_records(data['data']['data']['record_list'])
    except:
        print("failed to get records")
        print(json.dumps(data, indent=4))
        sleep(2)
        return

    df_new_list = []

    for idx, item in df.iterrows():
        if item['entry_mode'] == 1 and item['groups'] is not None:
            # recognized face
            df_new_list.append(item)
        if item['entry_mode'] == 3:
            # recognized card
            if idx == 0:
                #first item
                down, down_idx = find_nearest_temp_down(idx, item, df)
                temporary = item
                exceed5 = time_range_exceed_four_seconds(df, idx, down_idx)
                if not exceed5:
                    temporary['body_temperature'] = down
                df_new_list.append(temporary)

            elif idx == len(df) - 1:
                top, top_idx = find_nearest_temp_top(idx, item, df)
                temporary = item
                exceed5 = time_range_exceed_four_seconds(df, idx, top_idx)
                if not exceed5:
                    temporary['body_temperature'] = top
                df_new_list.append(temporary)

            else:
                top, top_idx = find_nearest_temp_top(idx, item, df)
                down, down_idx = find_nearest_temp_down(idx, item, df)
                flag_use_top = compare(idx, item, df, top_idx, down_idx)

                if flag_use_top == 1:
                    temporary = item
                    exceed5 = time_range_exceed_four_seconds(df, idx, top_idx)
                    if not exceed5:
                        temporary['body_temperature'] = top
                    df_new_list.append(temporary)
                else:
                    temporary = item
                    exceed5 = time_range_exceed_four_seconds(df, idx, down_idx)
                    if not exceed5:
                        temporary['body_temperature'] = down
                    df_new_list.append(temporary)

    df_new = pd.DataFrame(df_new_list)

    # print(datetime.fromtimestamp(df_new.iloc[0]['sign_time']).strftime('%Y-%m-%d %H:%M:%S'))

    # sys.stdout.write("\033[K")
    # print(df_new.iloc[0]['user_name'],
    #       df_new.iloc[0]['user_ic_number'],
    #       df_new.iloc[0]['body_temperature'],
    #       end='\r')

    #if today is a new day, then clear the remembered names
    if today != date.today():
        names = []
        today = date.today()
        main_gui.head_count_label.setText("人數: " + str(len(names)))

    # return if no data is retrieved
    if len(df_new.index) < 1:
        return

    # do not display QRCode
    if df_new.iloc[0]['entry_mode'] == 2:
        return
    
    # if new data is coming
    if current_name_temperature_datetime != (str(df_new.iloc[0]['user_name']), df_new.iloc[0]['body_temperature'], datetime.fromtimestamp(df_new.iloc[0]['sign_time'])):
        print(datetime.now())
        
        # update data
        prev_name_temperature_datetime2 = prev_name_temperature_datetime1
        prev_name_temperature_datetime1 = current_name_temperature_datetime
        current_name_temperature_datetime = (str(df_new.iloc[0]['user_name']), df_new.iloc[0]['body_temperature'], datetime.fromtimestamp(df_new.iloc[0]['sign_time']))
        name, temperature, _datetime = current_name_temperature_datetime
        
        #ignore first data read
        if first_data:
            first_data = False
            return

        # remember who checked-in
        if name not in names:
            names.append(name)

        # play alarm if needed on linux (need to install sox: sudo apt install sox)
        if temperature and temperature >= temperature_threshold:
            for i in range(4):
                os.system('play -nq -t alsa synth {} sine {}'.format(0.1, 880))
                sleep(0.05)
            os.system('play -nq -t alsa synth {} sine {}'.format(0.1, 880))

        #update UI
        if display:
            update_labels(main_gui.current_name_temperature_label, main_gui.current_time_label, *current_name_temperature_datetime)
            #update_labels(main_gui.prev1_name_temperature_label, main_gui.prev1_time_label, *prev_name_temperature_datetime1)
            #update_labels(main_gui.prev2_name_temperature_label, main_gui.prev2_time_label, *prev_name_temperature_datetime2)
            main_gui.head_count_label.setText("人數: " + str(len(names)))

        #send data to eclass if needed
        if lms == "eclass" and should_send_to_lms and df_new.iloc[0]['user_ic_number']:
            send_to_eclass(df_new)

        #send data to grwth if needed
        if lms == "grwth" and should_send_to_lms and df_new.iloc[0]['user_ic_number']:
            send_to_grwth(df_new)
        
def send_to_eclass(df):
    card_id = str(df.iloc[0]['user_ic_number']).zfill(10)
    record_date = str(df.iloc[0]['sign_date'])
    record_time = str(datetime.fromtimestamp(df.iloc[0]['sign_time']).strftime('%H:%M:%S'))
    request_type = "ATT"
    data = {"card_id": card_id,
            "record_date": record_date,
            "record_time": record_time,
            "request_type": request_type}

    # include temperature data if present
    temperature = df.iloc[0]['body_temperature']
    if temperature:
        temperature_status = 1 if temperature >= temperature_threshold else 0
        data["temperature"] = float(temperature)
        data["temperature_status"] = temperature_status

    # include location data if present
    location = df.iloc[0]['location']
    if location:
        data["location"] = location[:16]

    print("data:", json.dumps(data, indent=4))

    headers = {"Authorization": "Bearer " + eclass_licence_key}

    success = False
    retry_count = 0
    while not success:
        try:
            response = requests.post(eclass_api_endpoint, data=data, headers=headers)
            if response.status_code == 429 and retry_count < max_retry_number:
                # too many requests
                retry_count += 1
                sleep(1)
                continue
            success = True
        except requests.exceptions.ConnectionError:
            print(f"cannot connect to {eclass_api_endpoint}, check internet connection?")
            sleep(1)

    print(response)
    print(response.text)

def send_to_grwth(df):
    # grwth_school_id = os.getenv("GRWTH_SCHOOL_ID")
    # grwth_api_key = os.getenv("GRWTH_API_KEY")
    # grwth_api_endpoint = os.getenv("GRWTH_API_ENDPOINT")
    card_id = str(df.iloc[0]['user_ic_number']).zfill(10)
    record_date = str(df.iloc[0]['sign_date'])
    device_ldid = str(df.iloc[0]['device_ldid'])
    record_time = str(datetime.fromtimestamp(df.iloc[0]['sign_time']).strftime('%Y%m%d %H:%M:%S'))
    request_type = "ATT"
    data =  {
                "dat":[
                    {
                        "rid": device_ldid,
                        "cid": card_id,
                        "time": record_time
                    }
                ]
            }
    print("data:", json.dumps(data, indent=4))
    data = str(data)

    string_to_sign = f"{data}{grwth_school_id}{grwth_api_key}"
    print("string to sign:", string_to_sign)
    sign = hashlib.md5(string_to_sign.encode('utf-8')).hexdigest()

    headers = {
        "sign": sign,
        "schoolcode": grwth_school_id
    }

    print("headers:", headers)

    success = False
    retry_count = 0
    while not success:
        try:
            response = requests.post(grwth_api_endpoint, data=data, headers=headers)
            if response.status_code == 429 and retry_count < max_retry_number:
                # too many requests
                retry_count += 1
                sleep(1)
                continue
            success = True
        except requests.exceptions.ConnectionError:
            print(f"cannot connect to {grwth_api_endpoint}, check internet connection?")
            sleep(1)

    print(response)
    print(response.text)

if display:
    # Subclassing QThread
    # http://qt-project.org/doc/latest/qthread.html
    class Update_info_thread(QThread):
        def run(self):
            while True:
                all()
                # main_gui.position_on_screen() #somehow need this to prevent it from shifting for ykh
                sleep(0.5)
        
def update_labels(name_temperature_label, time_label, name, temperature, datetime):
    # name_temperature_label.setText(name + "\n" + str(temperature) + "°C")
    time_label.setText(datetime.strftime('%H:%M:%S'))

    #display name, temperature
    if not temperature:
        name_temperature_label.setText(name + "\n" + "")
    elif temperature >= temperature_threshold:
        name_temperature_label.setText(name + "\n" + "體溫異常")
    else:
        name_temperature_label.setText(name + "\n" + "體溫正常")

    # highlight name red for high temperature
    if not temperature:
        name_temperature_label.setStyleSheet("background-color: rgba(0,0,0,0%)")
    elif temperature >= temperature_threshold:
        name_temperature_label.setStyleSheet("background-color:#ff0000;")
    else:
        name_temperature_label.setStyleSheet("background-color: rgba(0,0,0,0%)")
    
    # highlight time green for late
    '''
    late_time_threshold = get_late_time_threshold()
    if datetime > late_time_threshold:
        time_label.setStyleSheet("background-color:#00ff00;")
    else:
        time_label.setStyleSheet("background-color: rgba(0,0,0,0%)")
    '''   

devices = get_devices()

try:
    current_device_id = devices[initial_device_index]['ldid']
except IndexError:
    print("device index " + str(initial_device_index) + " out of range, defaulting to 0")
    current_device_id = devices[0]['ldid']

if display:
    stylesheet = (
        "Main_GUI {"
            "border-image: url(" + background_image_path + ");"
        "}"
    )

    app = QApplication([])
    app.setStyleSheet(stylesheet)

    main_gui = Main_GUI()

    sleep(5)
    main_gui.device_selection_box.setCurrentIndex(initial_device_index)
    main_gui.position_on_screen()
    main_gui.show()

    thread = Update_info_thread()
    thread.finished.connect(app.exit)
    thread.start()
    app.exit(app.exec_())
else:
    #for running on Google App Engine
    from flask import Flask
    import threading
    import logging
    logging.basicConfig(level=logging.DEBUG)

    def run():
        while True:
            all()
            # main_gui.position_on_screen() #somehow need this to prevent it from shifting for ykh
            sleep(0.5)

    x = threading.Thread(target=run)

    app = Flask(__name__)
    @app.route('/')
    def hello():
        """Return a friendly HTTP greeting."""
        if not x.is_alive():
            x.start()
        print("Service Running~")
        app.logger.info('Service Running~~')
        return 'Service Running'
        
if __name__=="__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)