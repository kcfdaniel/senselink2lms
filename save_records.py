import schedule
import time
from datetime import datetime, timedelta
import os
import requests
import hashlib
import json
import pandas as pd
import random
import numpy as np
import sys
import urllib3
from dotenv import load_dotenv
load_dotenv()

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

#senselink address
senselink_host = os.getenv("SENSELINK_HOST")
#Input app key
app_key = os.getenv("APP_KEY")
#Input app secret
app_secret = os.getenv("APP_SECRET")
#API URL
uri = "/api/v3/record/list"

max_recursion_depth = 20

def getTimestamp():
    return str(round(time.time()) * 1000)

def getSign(timestamp):
    hl = hashlib.md5()
    hl.update((timestamp + '#' +app_secret).encode(encoding='utf-8'))
    return hl.hexdigest()

def find_nearest_temp_top(idx, item, df, depth=0):
    if idx == 0:
        #base case
        if item['body_temperature'] is not None and item['body_temperature'] != 0.0 and item['user_id'] == 0:
            return item['body_temperature'], 0
        else:
            return 0.0, 0
    elif df.iloc[idx-1]['body_temperature'] is not None and df.iloc[idx-1]['body_temperature'] != 0.0 and df.iloc[idx-1]['user_id'] == 0 and df.iloc[idx-1]['device_ldid'] == item['device_ldid']:
        #found target
        return df.iloc[idx-1]['body_temperature'], idx-1
    elif depth < max_recursion_depth:
        #try recursion
        return find_nearest_temp_top(idx-1, item, df, depth+1)
    else:
        #terminate
        return 0.0, 0

def find_nearest_temp_down(idx, item, df, depth=0):
    if idx == len(df)-1:
        #base case
        if item['body_temperature'] is not None and item['body_temperature'] != 0.0 and item['user_id'] == 0:
            return item['body_temperature'], len(df)-1
        else:
            return 0.0, len(df)-1
    elif df.iloc[idx+1]['body_temperature'] is not None and df.iloc[idx+1]['body_temperature'] != 0.0 and df.iloc[idx+1]['user_id'] == 0 and df.iloc[idx+1]['device_ldid'] == item['device_ldid']:
        #found target
        return df.iloc[idx+1]['body_temperature'], idx+1
    elif depth < max_recursion_depth:
        #try recursion
        return find_nearest_temp_down(idx+1, item, df, depth+1)
    else:
        #terminate
        return 0.0, 0

def compare(idx, item, df, top_idx, down_idx):

    top_time_diff = df.iloc[top_idx]['sign_time'] - item['sign_time']
    down_time_diff = item['sign_time'] - df.iloc[down_idx]['sign_time']

    if top_time_diff <= down_time_diff:
        return 1
    else:
        return 0

def time_range_exceed_four_seconds(df, idx, compare_idx):

    time_diff = abs(df.iloc[idx]['sign_time'] - df.iloc[compare_idx]['sign_time'])
    if time_diff >= 4:
        return True
    else:
        return False

def save_raw_data(df, file_name):
    df = df[[
        # 'id',
        # 'direction',
        # 'latitude',
        # 'longitude',
        # 'address',
        'location',
        # 'remark',
        #'groups',
        # 'mobile',
        'mask',
        # 'capture_picture',
        # 'capture_bg_picture',
        # 'avatar',
        # 'heat_avatar',
        'user_id',
        'user_name',
        'user_type',
        'group_id',
        'group_name',
        'device_name',
        'device_ldid',
        'sign_time',
        # 'country_code',
        # 'place_code',
        # 'on_business',
        'entry_mode',
        # 'sign_time_zone',
        # 'verify_score',
        # 'mis_id',
        # 'mis_type',
        # 'doc_photo',
        'ic_number',
        'id_number',
        # 'abnormal_type',
        # 'job_number',
        'user_ic_number',
        'user_id_number',
        # 'reception_user_id',
        # 'reception_user_name',
        'sign_date',
        # 'user_remark',
        'body_temperature',
        # 'delete_avatar',
        # 'create_at'
    ]]

    df.to_csv(f'{file_name}.csv', index=False)
    print(f"written {file_name}.csv")

def read_raw_data_file():
    df = pd.read_csv('raw.csv')
    df = df.where(pd.notnull(df), None)
    pd.to_numeric(df['entry_mode'], errors='ignore')
    return df

def all():

    now = datetime.now()
    # todays_date = now.strftime("%Y-%m-%d")
    todays_date = (now - timedelta(days=0)).strftime("%Y-%m-%d")
    #todays_strDate = datetime.strptime(todays_date, "%Y-%m-%d")

    # (3) add to provide alternative date-time format for file naming
    todays_date_time = (now - timedelta(days=0)).strftime("%Y-%m-%d_%H_%M_%S")
    #firstDate_selectedDayRange = (now - timedelta(days=user_input)).strftime("%Y-%m-%d")

    all_records_list = []
    page_num = 1

    while True:

        user_input = input("Please input a number for the required day range. (E.g. 0: current day ONLY, 1: previous day, 7: latest seven days)")
        user_input = int(user_input)

        if user_input not in range(0,32):
             print("Please input an appropriate number")
        else:
            if user_input == 0:
                print("you selected to extract today's records from Senselink.")
            else:
                print("you selected to extract ",user_input, " day(s) records from SenseLink.")
           # break

            timestamp = getTimestamp()
            sign = getSign(timestamp)

            url = senselink_host + uri
            data = {
                "app_key": app_key,
                "sign": sign,
                "timestamp": timestamp,
                "page": page_num,
                "size": 100,
                "date_time_from": todays_date + ' 00:00:00',
                #"data_time_from": (now - timedelta(days=user_input)).strftime("%Y-%m-%d") + ' 00:00:00',
                "date_time_to": todays_date + ' 23:59:59',
                #"data_time_to": now.strftime("%Y-%m-%d") + ' 23:59:59'
            }

            result = requests.get(url, params=data,verify=False)
            #result = requests.get(url, params=data,verify='/var/lib/docker/overlay2/5e226d905c2a6a879ed8528ce587cc3f11de09247a668e1d2b38ccbf70680167/merged/etc/nginx/certs/cdnis_key.key')
            #result = json.dumps(result.text, indent=4)

            #print(json.dumps(result.text, indent=4))
            print("The attendance records are now retrieving from SenseLink, processing takes time, please wait patiently...")
            print(json.dumps(result.text, indent=4))

            data = json.loads(result.text, encoding = 'utf_8_sig')

            if not data['data']['data']['record_list']:
                break

            #df = pd.read_json(data['data']['data']['record_list'])

            current_records_batch_dataframe = pd.DataFrame.from_records(data['data']['data']['record_list'])

            all_records_list.append(current_records_batch_dataframe)

            page_num += 1

        df = pd.concat(all_records_list, ignore_index=True)

        # # writing raw data for testing
        # save_raw_data(df, 'raw')
        # return

        # # reading raw data from local for testing
        # df = read_raw_data_file()

        processed_records_list = []

        for idx, item in df.iterrows():
            if item['entry_mode'] == 1 and item['groups'] is not None:
                # recognized face
                processed_records_list.append(item)
            if item['entry_mode'] == 3:
                # recognized card
                if idx == 0:
                    #first item
                    down, down_idx = find_nearest_temp_down(idx, item, df)
                    temporary = item
                    exceed5 = time_range_exceed_four_seconds(df, idx, down_idx)
                    if not exceed5:
                        temporary['body_temperature'] = down
                    processed_records_list.append(temporary)

                elif idx == len(df) - 1:
                    #last item
                    top, top_idx = find_nearest_temp_top(idx, item, df)
                    temporary = item
                    exceed5 = time_range_exceed_four_seconds(df, idx, top_idx)
                    if not exceed5:
                        temporary['body_temperature'] = top
                    processed_records_list.append(temporary)

                else:
                    top, top_idx = find_nearest_temp_top(idx, item, df)
                    down, down_idx = find_nearest_temp_down(idx, item, df)
                    flag_use_top = compare(idx, item, df, top_idx, down_idx)

                    if flag_use_top == 1:
                        temporary = item
                        exceed5 = time_range_exceed_four_seconds(df, idx, top_idx)
                        if not exceed5:
                            temporary['body_temperature'] = top
                        processed_records_list.append(temporary)
                    else:
                        temporary = item
                        exceed5 = time_range_exceed_four_seconds(df, idx, down_idx)
                        if not exceed5:
                            temporary['body_temperature'] = down
                        processed_records_list.append(temporary)

        if len(processed_records_list) == 0:
            print("no records today")
            pd.DataFrame().to_csv(f'{todays_date_time}.csv', index=False)
            return

        processed_records_dataframe = pd.DataFrame(processed_records_list)

        # education_bureau = processed_records_dataframe[['user_name','user_ic_number','sign_time', 'body_temperature']]
        # education_bureau['sign_time'] = education_bureau['sign_time'].apply(lambda t: datetime.fromtimestamp(t))
        # education_bureau.to_csv('education_bureau.csv', index=False)

        processed_records_dataframe = processed_records_dataframe[[
            # 'id',
            # 'direction',
            # 'latitude',
            # 'longitude',
            # 'address',
            'location',
            # 'remark',
            # 'groups',
            # 'mobile',
            #'mask',
            # 'capture_picture',
            # 'capture_bg_picture',
            # 'avatar',
            # 'heat_avatar',
            'user_id',
            'user_name',
            'user_type',
            #'group_id',
            #'group_name',
            #'device_name',
            #'device_ldid',
            'sign_time',
            # 'country_code',
            # 'place_code',
            # 'on_business',
            # 'entry_mode',
            # 'sign_time_zone',
            # 'verify_score',
            # 'mis_id',
            # 'mis_type',
            # 'doc_photo',
            'ic_number',
            'id_number',
            #'abnormal_type',
            'job_number',
            'user_ic_number',
            'user_id_number',
            # 'reception_user_id',
            # 'reception_user_name',
            'sign_date',
            # 'user_remark',
            'body_temperature',
            # 'delete_avatar',
            # 'create_at'
        ]]
        processed_records_dataframe['sign_time'] = processed_records_dataframe['sign_time'].apply(lambda t: (datetime.fromtimestamp(t)).strftime("%m-%d-%Y %T"))

        #processed_records_dataframe['sign_date'] = processed_records_dataframe['sign_date'].strftime("%Y-%m-%d") -> Error: Series object do not have attribute .strftime
        #processed_records_dataframe['sign_date'] = processed_records_dataframe['sign_date'].apply(lambda t: t.strftime("%Y-%m-%d"))

        # Q2 handle selected day range
        if user_input in range (0,32):
            firstDate_selectedDayRange = (now - timedelta(days=user_input)).strftime("%Y-%m-%d")
            minDate = datetime.strptime(firstDate_selectedDayRange, "%Y-%m-%d")
            drop = []

            for i in processed_records_dataframe['sign_date']:
                current_record = datetime.strptime(i, "%Y-%m-%d")
                if current_record < minDate:
                    drop.append(i)

            processed_records_dataframe = processed_records_dataframe[~processed_records_dataframe['sign_date'].isin(drop)]

        # (5) add 'Department column' in the output csv; users can perform sorting in excel by 'Department'
        dept = []
        jobNO = processed_records_dataframe['job_number']
        for i in jobNO:
            if str(i)[0] == 'P':
                dept.append("Staffs")
            elif str(i)[0] == 'T':
                dept.append("Contractors")
            else:
                dept.append("Students")
        processed_records_dataframe['Department'] = dept

        processed_records_dataframe.to_csv(f'{todays_date_time}.csv', index=False)
        print(f"written {todays_date_time}.csv")
        #dt_object = datetime.fromtimestamp(processed_records_dataframe.iloc[0]['sign_time'])

        #(1) add manually terminate program after a csv file is generated
        while True:
            user_decision = input("The csv file has been generated, do you want to terminate the program ? (y/n)")
            if user_decision == 'y':
                sys.exit()
                break
            elif user_decision == 'n':
                print("The program will keep executing.")
                break
            else:
                print("Wrong input. Please press y to determine program or n to keep it runnin AGAIN.")
                continue

all()
#Q1 - automatically terminate program after csv file is generated, just comment the codes below
'''
schedule.every().day.at("00:00").do(all)

while True:
    schedule.run_pending()
    time.sleep(1)
'''
