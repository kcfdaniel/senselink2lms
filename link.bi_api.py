from time import time, sleep
import os
import requests
import hashlib
import json
import numpy as np
import sys
from threading import Thread
import os
from datetime import datetime, date, timedelta
import pytz
from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_socketio import SocketIO, send, emit
import socket
parser = reqparse.RequestParser()
import traceback
import argparse
from dotenv import load_dotenv
load_dotenv()
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

print()
print("started at", datetime.now())

ap = argparse.ArgumentParser()
ap.add_argument("-r", "--remote",
                required=False,
                default=True,
                action='store_true',
                help="executing in a different subnet in regards to SenseLink")
ap.add_argument("-l", '--local',
                required=False,
                dest="remote",
                action='store_false',
                help="executing in the same subnet in regards to SenseLink")
ap.add_argument("-v", '--verbose',
                required=False,
                default=False,
                action='store_true',
                help="print more messages for debugging")

args = vars(ap.parse_args())

# senselink host
senselink_host = os.getenv("SENSELINK_HOST")
# host port
host_port = int(os.getenv("HOST_PORT"))
# Input app key
app_key = os.getenv("APP_KEY")
# Input app secret
app_secret = os.getenv("APP_SECRET")

# subscribe uri
subscribe_uri = "/api/v3/event/updateSub"
# check subscription uri
check_subscription_uri = "/api/v3/event/viewSub"

# API URL
uri = "/api/v3/record/list"
# number of times to retry sending data to senselink
max_retry_number = 5
# executing remotely or locally in regards to senselink
remote = args["remote"]
# verbose: print more debug messages
verbose = args["verbose"]

def get_timestamp():
    return str(round(time()) * 1000)

def get_sign(timestamp):
    hl = hashlib.md5()
    hl.update((timestamp + '#' +app_secret).encode(encoding='utf-8'))
    return hl.hexdigest()

def call_senselink_api(url, method='get', params={}, data={}, verbose=False):
    timestamp = get_timestamp()
    sign = get_sign(timestamp)
    auth_params = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
    }
    params = {**params, **auth_params}
    success = False
    retry_count = 0
    connection_error = False
    while not success:
        try:
            if method == "get":
                response = requests.get(url, params=params, timeout=1,verify=False)
            elif method == "post":
                response = requests.post(url, params=params, json=data, timeout=1)
            else:
                print(f"method {method} not supported when calling senselink api")
            if (response.status_code == 429 or response.status_code == 502) and retry_count < max_retry_number:
                retry_count += 1
                sleep(1)
                continue
            success = True
        except requests.exceptions.ConnectionError:
            if not connection_error:
                connection_error = True
                print(f"cannot connect to {url}, retrying", end="")
            else:
                print(".", end="")
            sleep(1)
        except requests.exceptions.Timeout:
            if retry_count < max_retry_number:
                retry_count += 1
                print("request timeout, trying again")
            else:
                print("ERROR: multiple request timeouts, giving up")
                success = True
    try:
        if verbose:
            print(json.loads(response.text))
        return json.loads(response.text)
    except json.decoder.JSONDecodeError:
        return {}


def subscribe():
    print("subscribe!")
    timestamp = get_timestamp()
    sign = get_sign(timestamp)
    url = senselink_host + subscribe_uri
    params = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
    }

    if remote:
        ip = requests.get('https://checkip.amazonaws.com',verify=False).text.strip()
    else:
        def get_ip_address():
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("8.8.8.8", 80))
            return s.getsockname()[0]
        ip = get_ip_address()

    data = {
        "event_type_ids": [30000, 30100],
        "event_dest": f"http://{ip}:{host_port}/eventRcv"
    }

    # try to subscribe to SenseLink
    success = False
    retry_count = 0
    connection_error = False
    while not success:
        try:
            response = requests.post(url, params=params, json=data)
            print(response)
            print(response.text)
            if (response.status_code == 429 or response.status_code == 502) and retry_count < max_retry_number:
                # too many requests or bad gateway
                retry_count += 1
                sleep(1)
                continue
            success = True
        except requests.exceptions.ConnectionError:
            if not connection_error:
                connection_error = True
                print(f"cannot connect to {url}, retrying", end="")
            else:
                print(".", end="")
            sleep(1)

def check_subscription():
    timestamp = get_timestamp()
    sign = get_sign(timestamp)
    url = senselink_host + check_subscription_uri
    params = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
    }

    result = requests.get(url, params=params,verify=False)
    data = json.loads(result.text)
    print(data)

subscribe()

app = Flask(__name__)
api = Api(app)
socketio = SocketIO(app, cors_allowed_origins="*")

class Receive_Event_Resource(Resource):
    def post(self):
        event = request.get_json(force=True)
        # if device alarm
        if event['eventType'] == 30100 and event['data']['code'] == 20003 and event['data']['status'] == 1:
            print()
            print("FIRE ALARM!")
            data = event['data']
            if os.name == "nt":
                # avoid error on windows
                print("event received:", str(data).encode('utf-8'))
            else:
                print("event received:", data)
            handle_fire_alarm(event['data']['deviceId'])


api.add_resource(Receive_Event_Resource, '/eventRcv')

def handle_fire_alarm(device_id):
    url = senselink_host + "/api/v3/device"
    response = call_senselink_api(url=url)

    if not response:
        print("ERROR: cannot get device list")
        return

    device_list = response['data']['device_list']
    online_devices = [device for device in device_list if device["state"] == 1]
    print("online devices", [device['name'] for device in online_devices])
    open_devices_door(device_id, online_devices)

def open_devices_door(alarm_device_id, devices=[]):
    if not devices:
        print("Warning: devices is empty when calling open_devices_door")
        return
    url = senselink_host + "/api/v2/device/open"
    while True:
        for device in devices:
            print("opening door for device", device['name'])
            params = {"id": device['id']}
            Thread(
                target=call_senselink_api, 
                kwargs={
                    'url':url,
                    'method':"post", 
                    'params':params, 
                    'verbose': True
                    }
                ).start()
        sleep(3) # sleep 3 seconds ok for 1 device

        #check if alarm is still on
        check_alarm_status_url = senselink_host + "/api/v2/device/alarm/list"
        params = {
            "deviceId": alarm_device_id,
            "code": 20003,
        }
        response = call_senselink_api(url=check_alarm_status_url, method="get", params=params)
        if response:
            print("latest alarm",  response['data']['data'][0])
            current_alarm_code = response['data']['data'][0]['code']
            current_alarm_id = response['data']['data'][0]['traceId']
            current_alarm_status = response['data']['data'][0]['status']
            current_alarm_timestamp = response['data']['data'][0]['alarmTimeL']

            #while current_alarm_id is not None:
            if current_alarm_status == 1 and current_alarm_code == 20003:
                if int(get_timestamp()) - current_alarm_timestamp > 5  : # alarm still remains alert after 5 seconds
                    if current_alarm_id is None:
                        break

            if current_alarm_status == 3:
                print("Fire alarm is dismissed")
                break
        else:
            print("ERROR: failed to query alarms")
            return

@socketio.on('connect')
def on_client_connect():
    print("Client connected")

@socketio.on('disconnect')
def on_client_disconnect():
    print('Client disconnected')

socketio.run(app, port=host_port, host='0.0.0.0')
