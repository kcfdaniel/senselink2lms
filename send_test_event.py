from time import time, sleep
import os
import requests
import hashlib
import json
import random
import numpy as np
import sys
import os
from datetime import datetime, date, timedelta
import pytz
import traceback
import argparse
from dotenv import load_dotenv
from random import randrange
load_dotenv()

print()
print("started at", datetime.now())

ap = argparse.ArgumentParser()
ap.add_argument("-c", "--card-detect", 
                required=False, 
                default=True, 
                action='store_true',
                help="simulate card detection")
ap.add_argument("-t", '--temperature-detect', 
                required=False, 
                dest="card_detect", 
                action='store_false',
                help="simulate temperature detection")
ap.add_argument("-i", '--ic-number', 
                required=False, 
                default='0008789878', 
                help="ic card number")

args = vars(ap.parse_args())

#host port
host_port = int(os.getenv("HOST_PORT"))
#senselink_host
# senselink_host = os.getenv("SENSELINK_HOST")
senselink_host = f"http://127.0.0.1:{host_port}" #for sending directly to the subscribed service
#Input app key
app_key = os.getenv("APP_KEY")
#Input app secret
app_secret = os.getenv("APP_SECRET")
#Image path
path = ""

#subscribe uri
subscribe_uri = "/api/v3/event/updateSub"
#check subscription uri
check_subscription_uri = "/api/v3/event/viewSub"
#send test event
# send_test_event_uri = "/api/v3/event/sendTest"
send_test_event_uri = "/eventRcv" #for sending directly to the subscribed service

#if it is a card detection event
card_detect = args["card_detect"]
#ic card number
ic_number = args["ic_number"]

def getTimestamp():
    return str(round(time()) * 1000)

def getSign(timestamp):
    hl = hashlib.md5()
    hl.update((timestamp + '#' +app_secret).encode(encoding='utf-8'))
    return hl.hexdigest()

def subscribe():
    timestamp = getTimestamp()
    sign = getSign(timestamp)
    url = senselink_host + subscribe_uri
    params = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
    }

    data = {
        "event_type_ids": [30000],
        "event_dest": f"http://223.18.75.253:{host_port}/eventRcv"
    }

    result = requests.post(url, params=params, json=data)
    data = json.loads(result.text)
    print(data)

def check_subscription():
    timestamp = getTimestamp()
    sign = getSign(timestamp)
    url = senselink_host + check_subscription_uri
    params = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
    }

    result = requests.get(url, params=params)
    data = json.loads(result.text)
    print(data)

def get_test_json_data():
    return {
        'messageId': f'{str(randrange(10**8)).zfill(8)}-{str(randrange(10**4)).zfill(4)}-{str(randrange(10**4)).zfill(4)}-{str(randrange(10**4)).zfill(4)}-{str(randrange(10**12)).zfill(12)}',
        'eventType': 30000,
        'sendTime': str(int(time())),
        'data': {
            'id': 0,
            'userId': 0,
            'name': 'test',
            'type': 2,
            'direction': 0,
            'verifyScore': 0.993,
            'groups': [{
                'id': 2,
                'name': '默认组',
                'type': 2
            }],
            'deviceName': 'test-1',
            'sn': "SPSE-ef69369cbba374a1eaf671ef24bbb9ce",
            'signDate': '2019-10-14',
            'signTime': str(int(time())),
            'companyId': 1723,
            'icNumber': ic_number if card_detect else '',
            'idNumber': '',
            'userIcNumber': '', 
            'userIdNumber': '', 
            'bodyTemperature': 0.0 if card_detect else 36.5,
            'jobNumber': '',
            'entryMode': 1,
            'signTimeZone': '+08:00',
            'location': '306',
            'abnormalType': 0
        }
    }

def send_test_event():
    timestamp = getTimestamp()
    sign = getSign(timestamp)
    url = senselink_host + send_test_event_uri
    params = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
    }

    result = requests.post(url, params=params, json=get_test_json_data()) #for sending directly to the subscribed service
    # result = requests.get(url, params=params)
    # print(result.text)
    data = json.loads(result.text)
    print(data)

# subscribe()
# check_subscription()

send_test_event()