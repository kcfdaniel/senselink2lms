import pandas as pd
import numpy as py
import shutil

file_name = 'path_to_xlsx_file'
file = pd.read_excel(file_name)

for name in file['ChineseName']:
    shutil.copy("path_to_default_face_image","path_to_store_generated_images/{}.png".format(name))
