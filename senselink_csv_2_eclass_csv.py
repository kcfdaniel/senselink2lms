import time
from datetime import datetime
import os
import requests
import hashlib
import json
import pandas as pd
import random
import numpy as np
import sys
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-f", "--format", 
                required=False, 
                default="api",
                help="format of data to save in")
ap.add_argument("-a", "--api", 
                required=False, 
                dest="format", 
                action='store_const',
                const="api",
                help="save data in api format")
ap.add_argument("-b", '--batch', 
                required=False, 
                dest="format", 
                action='store_const',
                const="batch",
                help="save data in format for teacher to batch add")

args = vars(ap.parse_args())

format = args["format"]

def find_nearest_temp_top(idx, item, df):
    if idx == 0:
        if item['body_temperature'] is not None and item['body_temperature'] != 0.0 and item['user_id'] == 0:
            return item['body_temperature'], 0
        else:
            return 37, 0
    elif df.iloc[idx-1]['body_temperature'] is not None and df.iloc[idx-1]['body_temperature'] != 0.0 and df.iloc[idx-1]['user_id'] == 0 :
        return df.iloc[idx-1]['body_temperature'], idx-1
    else:
        return find_nearest_temp_top(idx-1, item, df)

def find_nearest_temp_down(idx, item, df):
    if idx == len(df)-1:
        if item['body_temperature'] is not None and item['body_temperature'] != 0.0 and item['user_id'] == 0:
            return item['body_temperature'], len(df)-1
        else:
            return 37, len(df)-1
    elif df.iloc[idx+1]['body_temperature'] is not None and df.iloc[idx+1]['body_temperature'] != 0 and df.iloc[idx+1]['user_id'] == 0 :
        return df.iloc[idx+1]['body_temperature'], idx+1
    else:
        return find_nearest_temp_down(idx+1, item, df)

def compare(idx, item, df, top_idx, down_idx):
    top_time_diff = df.iloc[top_idx]['sign_time'] - item['sign_time']
    down_time_diff = item['sign_time'] - df.iloc[down_idx]['sign_time']

    if top_time_diff <= down_time_diff:
        return 1
    else:
        return 0

def time_range_exceed_four_seconds(df, idx, compare_idx):
    time_diff = abs(df.iloc[idx]['sign_time'] - df.iloc[compare_idx]['sign_time'])
    if time_diff >= 4:
        return True
    else:
        return False

try:
    df = pd.read_csv('records.csv')
except FileNotFoundError:
    print("Error: records.csv not found, ensure that you have this file in the same directory as this script")
    sys.exit()

df = df.rename(columns={'Verification method': 'entry_mode'})
df = df.rename(columns={'Group': 'groups'})
df = df.rename(columns={'Body Temperature': 'body_temperature'})
df = df.rename(columns={'Time': 'sign_time'})
df = df.rename(columns={'IC Card No.(Read by device)': 'ic_number'})
df = df.rename(columns={'Device': 'location'})

df['user_id'] = pd.Series([0 for x in range(len(df.index))])
df['entry_mode'] = df['entry_mode'].replace(to_replace = "Face", value = 1) 
df['entry_mode'] = df['entry_mode'].replace(to_replace = "QR Code", value = 2) 
df['entry_mode'] = df['entry_mode'].replace(to_replace = "Card", value = 3)
df['sign_time'] = df['sign_time'].apply(lambda t: datetime.timestamp(datetime.strptime(t, '%Y-%m-%d %H:%M:%S')))

datetime_object = datetime.strptime('Jun 1 2005  1:33PM', '%b %d %Y %I:%M%p')

# print(df['sign_time'])
df_new_list = []

for idx, item in df.iterrows():
    if item['entry_mode'] == 1 and item['groups'] and item['ic_number'] and not np.isnan(item['ic_number']):
        df_new_list.append(item)
    if item['entry_mode'] == 3:
        if idx == 0:
            down, down_idx = find_nearest_temp_down(idx, item, df)
            temporary = item
            exceed5 = time_range_exceed_four_seconds(df, idx, down_idx)
            if not exceed5:
                temporary['body_temperature'] = down
            df_new_list.append(temporary)

        elif idx == len(df) - 1:
            top, top_idx = find_nearest_temp_top(idx, item, df)
            temporary = item
            exceed5 = time_range_exceed_four_seconds(df, idx, top_idx)
            if not exceed5:
                temporary['body_temperature'] = top
            df_new_list.append(temporary)

        else:
            top, top_idx = find_nearest_temp_top(idx, item, df)
            down, down_idx = find_nearest_temp_down(idx, item, df)
            flag_use_top = compare(idx, item, df, top_idx, down_idx)

            if flag_use_top == 1:
                temporary = item
                exceed5 = time_range_exceed_four_seconds(df, idx, top_idx)
                if not exceed5:
                    temporary['body_temperature'] = top
                df_new_list.append(temporary)
            else:
                temporary = item
                exceed5 = time_range_exceed_four_seconds(df, idx, down_idx)
                if not exceed5:
                    temporary['body_temperature'] = down
                df_new_list.append(temporary)

df_new = pd.DataFrame(df_new_list)

# education_bureau = df_new[['user_name','user_ic_number','sign_time', 'body_temperature']]
# education_bureau['sign_time'] = education_bureau['sign_time'].apply(lambda t: datetime.fromtimestamp(t))
# education_bureau.to_csv('education_bureau.csv', index=False)

if format == "api":
    # for calling api
    eclass = df_new[['ic_number', 'location', 'body_temperature']]
    eclass['ic_number'] = df_new['ic_number'].apply(lambda ic_number: str(int(ic_number)).zfill(10))
    eclass['date'] = df_new['sign_time'].apply(lambda t: (datetime.fromtimestamp(t)).strftime("%Y-%m-%d"))
    eclass['time'] = df_new['sign_time'].apply(lambda t: (datetime.fromtimestamp(t)).strftime("%T"))
    eclass = eclass.rename(columns={'location': 'Site'})
    eclass = eclass.rename(columns={'ic_number': 'Card ID'})
    eclass = eclass.rename(columns={'body_temperature': 'Temperature'})
    eclass = eclass.rename(columns={'time': 'Time'})
    eclass = eclass.rename(columns={'date': 'Date'})
    eclass.to_csv('eclass.csv', index=False, encoding = 'utf_8_sig')
elif format == "batch":
    # for teachers to batch add
    eclass = df_new[['sign_time', 'location', 'ic_number']]
    eclass['ic_number'] = df_new['ic_number'].apply(lambda ic_number: str(int(ic_number)))
    eclass['sign_time'] = df_new['sign_time'].apply(lambda t: (datetime.fromtimestamp(t)).strftime("%Y-%m-%d %H:%M"))
    eclass = eclass.rename(columns={'location': 'Site'})
    eclass = eclass.rename(columns={'ic_number': 'Card ID'})
    eclass = eclass.rename(columns={'sign_time': 'Time'})
    eclass.to_csv('eclass.csv', index=False, sep='\t', encoding = 'utf_8_sig')
else:
    print('error: output format not supported')